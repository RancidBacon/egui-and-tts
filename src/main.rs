
use std::collections::HashMap;


use tts::*;


// via <https://github.com/emilk/egui/blob/fc8b27807cfa3028f8b5128575a945b8f594fe9d/eframe/examples/hello_world.rs#L1>

use eframe::{egui, epi};


#[derive(Default,Debug)]
struct WidgetTtsState {

    has_spoken_for_keyboard_nav: bool,

    _suppress_tts_until: f64, // future time in seconds

}


impl WidgetTtsState {

    fn is_tts_suppressed(&self, current_time_seconds: f64) -> bool {
        // for hover
        current_time_seconds < self._suppress_tts_until
    }


    fn suppress_tts_until(&mut self, time_seconds: f64) {
        self._suppress_tts_until = time_seconds;
    }

}



trait TtsSupport {

    // TODO: Don't pass so many args around.
    fn handle_tts(self, tts: &mut TTS, ctx: &egui::CtxRef, tts_state: &mut WidgetTtsState, label: &str, button_text: &str) -> Self;
}


impl TtsSupport for egui::Response {


    fn handle_tts(self, tts: &mut TTS, ctx: &egui::CtxRef, tts_state: &mut WidgetTtsState, label: &str, button_text: &str) -> Self {

        if ctx.memory().has_kb_focus(self.id) {

            if !tts_state.has_spoken_for_keyboard_nav {
                tts_state.has_spoken_for_keyboard_nav = true;
                tts.speak(button_text, true).expect("TTS error occurred."); // NOTE: `text` doesn't appear to be retrievable?
            };

            // TODO: Handle in a way that merges mouse/key action.
            if ctx.input().key_pressed(egui::Key::Enter) {
                tts.speak(format!("Pressed {}", button_text), true).expect("TTS error occurred.");
            }

        } else if ctx.memory().lost_kb_focus(self.id) {
            tts_state.has_spoken_for_keyboard_nav = false;
        }

        if !self.hovered() {
            tts_state.suppress_tts_until(0.0);
        }


        self.on_hover_ui(|ui| {
            ui.label(label);

            if !tts_state.is_tts_suppressed(ctx.input().time) {

                tts.speak(label, true).expect("TTS error occurred."); // TODO: Handle errors better?

                tts_state.suppress_tts_until(ctx.input().time + 2.0); // TODO: Also suppress when repainted without mouse movement.

            }
        })

    }
}



struct WhaTTSApp {

    widget_tts_state: HashMap<egui::Id, WidgetTtsState>,

    first_run: bool, // TODO: Use warm up instead?

    tts: TTS,

}


// Also via: <https://github.com/emilk/egui/blob/fc8b27807cfa3028f8b5128575a945b8f594fe9d/epi/src/lib.rs#L57>

impl epi::App for WhaTTSApp {

    fn name(&self) -> &str {
        "WhaTTSApp"
    }



    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut epi::Frame<'_>) {


        egui::CentralPanel::default().show(ctx, |ui| {

            ui.horizontal(|ui| {


                // Ideally, the TTS would just happen automatically when,
                // e.g. the hover text is set like this:
                //
                //   let btn1 = btn1.on_hover_text("Button One");

                const BUTTON_TEXT_ONE: &str = "Button One";
                const BUTTON_TOOLTIP_ONE: &str =  "First Button";

                let button_text = BUTTON_TEXT_ONE;

                let btn1 = ui.button(button_text);

                let btn1_tts_state = self.widget_tts_state.entry(btn1.id).or_insert(WidgetTtsState::default());

                ctx.memory().interested_in_kb_focus(btn1.id);

                if self.first_run {
                    ctx.memory().request_kb_focus(btn1.id);
                    self.first_run = false;
                }


                let btn1 = btn1.handle_tts(&mut self.tts, ctx, btn1_tts_state, BUTTON_TOOLTIP_ONE, button_text);


                if btn1.clicked() {
                    self.tts.speak(format!("Clicked {}", button_text), true).expect("TTS error occurred.");
                };



                const BUTTON_TEXT_TWO: &str = "Button Two";
                const BUTTON_TOOLTIP_TWO: &str =  "Second Button";

                let button_text = BUTTON_TEXT_TWO;

                let btn2 = ui.button(button_text);

                let btn2_tts_state = self.widget_tts_state.entry(btn2.id).or_insert(WidgetTtsState::default());

                ctx.memory().interested_in_kb_focus(btn2.id);


                let btn2 = btn2.handle_tts(&mut self.tts, ctx, btn2_tts_state, BUTTON_TOOLTIP_TWO, button_text);

                if btn2.clicked() {
                    self.tts.speak(format!("Clicked {}", button_text), true).expect("TTS error occurred.");
                };

            });


        });


    }
}


fn main() -> Result<(), Error> {

    eframe::run_native(Box::new(WhaTTSApp {widget_tts_state: HashMap::new(), tts: TTS::default()?, first_run: true} ));

}

}
