# `egui` + `tts` proof-of-concept

Small proof-of-concept for combining the `tts` Text To Speech crate with the
`egui` immediate mode GUI crate. Part of investigating ways to make `egui`
more accessible.

For additional context, see: <https://github.com/emilk/egui/issues/167#issuecomment-780631695>


### Features

 * Two `egui` buttons.

 * When the mouse is hovered over a button its tooltip is spoken via TTS.

 * Keyboard navigation via TAB / Shift-TAB key.

 * When keyboard navigation is used, navigating to a button speaks its label.

 * Speech when a button is pressed (via Enter) or clicked.

 * Some attempt to not have a cacophony of sound through use of "cool down" etc.

 * Code written by someone still learning both Rust & idiomatic Rust. :)



## Prerequisites

On Linux you may slso need to install these packages via your package manager:

 * `clang` and/or `libclang1`

 * `libspeechd-dev`


### Tested configurations

Tested with (pre-installed):

 * `speech-dispatcher`

 * `espeak-ng` (AFAICT from output of `spd-say --list-output-modules`).

 * elementary OS 5.1.7 Hera (Built on Ubuntu 18.04.4 LTS / Linux 5.4.0-64-generic / GTK 3.22.30)


## License

MIT

----

Brought to you by: RancidBacon.com
